### SPA 单页面应用。
    . 只有一个真实页面。没有页面跳转，根据不同的路由去切换不同的路由组件。
    . 路由组件内容可能会向后台发送请求
    
### 前端路由: vue-router
    . 下载vue-router 
       npm install vue-router --save
    
    . 路由器: 用于创建路由器的构建函数
         new VueRouter({
         });
         
    . 路由:
        概念:
           路由就是键值对, 一种映射关系。
           key: 路由path, 
           value: 前端路由 对应的是组件
                  后台路由 回调函数
        routes: [
            { 
               path: "/"
               component: Index
            },
            { 
               path: "/about"
               component: About
            },
        ]
             

### 路由定义步骤:
    . 1. 定义路由组件
    . 2. 注册路由
    . 3. 使用路由标签
