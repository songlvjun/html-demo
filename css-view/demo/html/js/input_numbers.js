function isValueNumber(value) {
    return (/(^-?[0-9]+\.{1}\d+$) | (^-?[1-9][0-9]*$) | (^-?0{1}$)/).test(value + '');
}

/**
 input输入框的值绑定了currentValue 和 原生change事件
 */
Vue.component("input-number", {
    template: `
        <div class="input-number">
            <input type="text" :value="currentValue" @change="handleChange" @keyup.up="handleUp" 
             @keyup.down="handleDown"  />
            <button @click="handleDown" :disabled="currentValue<=min">-</button>
            <button @click="handleUp" :disabled="currentValue>=max">+</button>
	    </div>
    `,
    props: {
        max: {
            type: Number,
            default: Infinity // 正无限大
        },
        min: {
            type: Number,
            default: -Infinity // 负无限大
        },
        value: {
            type: Number,
            default: 0
        }
    },
    // watch用来监听某个prop或者data的变化
    watch: {
        // 监听value,是为了知道父组件修改了value
        value: function (val) {
            this.updateValue(val);
        },
        currentValue: function (val) {
            this.$emit('input', val); //在使用v-model时改变value的
            this.$emit('on-change', val);
        }
    },
    // Vue的组件是单向数据流，所以无法从内部直接修改prop的value值,在组件内部维护这个data
    data: function () {
        return {
            // 这只是解决了初始化引用组件value的问题,如果value值改变, 组件的currentValue也需要改变
            currentValue: this.value,
        }
    },
    methods: {
        updateValue: function (val) {
            if (val > this.max) {
                val = this.max;
            } else if (val < this.min) {
                val = this.min;
            } else {
                this.currentValue = val;
            }
        },
        handleDown: function () {
            if (this.currentValue <= this.min) return;
            this.currentValue--;
        },
        handleUp: function () {
            if (this.currentValue > this.max) return;
            this.currentValue++;
        },
        handleKeydown: function (event) {
            if (event.keyCode == 38) {
                this.handleUp();
            }
            if (event.keyCode == 40) {
                this.handleDown();
            }
        },
        handleChange: function (event) {
            var val = event.target.value.trim();
            var max = this.max;
            var min = this.min;

            console.log(this.currentValue);
            console.log(val);

          /*  val = Number(val);
            this.currentValue = val;

            if (val > max) {
                this.currentValue = max;
            } else if (val < min) {
                this.currentValue = min;
            }*/
        }
    },
    // 在实例初始化时, 对value进行过滤
    mounted: function () {
        this.updateValue(this.value);
    }
});