var Time = {

    // 获得当前时间戳
    getUnix: function () {
        var date = new Date();
        return date.getTime();
    },

    // 补齐时间格式
    handleTime: function (val) {
        return val < 10 ? "0" + val : val;
    },

    // 获得今天0点0分0秒的时间戳
    getTodayUnix: function () {
        var date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        return date.getTime();
    },

    // 获得今年1月1日0点0分0秒的时间戳
    getYearUnix: function () {
        var date = new Date();
        date.setMonth(0);
        date.setDate(1);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        return date.getTime();
    },

    // 获得标准年月日
    getLastDate: function (time) {
        var date = new Date(time);
        var month = this.handleTime(date.getMonth() + 1);
        var day = this.handleTime(date.getDate());
        return date.getFullYear() + "-" + month + "-" + day;
    },

    // 转换时间
    getFormatTime: function (timestamp) {
        // 当前的时间戳
        var now = this.getUnix();

        // 今天0点的时间戳
        var today = this.getTodayUnix();

        /**
         1分钟以前显示 刚刚
         1分钟到1小时之间 显示 xx 分钟之前
         1小时—— 1天之间  显示  xx 小时之前
         1天  - 31天之间  显示  xx 天之前
         大于 1 个月 显示 xx 年 xx 月 xx 日
         */

        var timer = (now - timestamp) / 1000;
        var tip = '';

        // 1分钟以前，显示“刚刚”。
        // 1分钟~1小时之间，显示“xx分钟前”。
        // 1小时~1天之间，显示“xx小时前”。
        // 1天~1个月（31天）之间，显示“xx天前”。
        // 大于1个月，显示“xx年xx月xx日”。

        if (timer <= 0) {
            tip = '刚刚';
        } else if (Math.floor(timer / 60) <= 0) {
            tip = '刚刚'
        } else if (timer < 3600) {
            tip = Math.floor(timer / 60) + '分钟前';
        } else if (timer >= 3600 && (timestamp - today >= 0)) {
            tip = Math.floor(timer / 3600) + '小时前'
        } else if (timer / 86400 <= 31) {
            tip = Math.ceil(timer / 86400) + '天前'
        } else {
            tip = this.getLastDate(timestamp);
        }
        return tip;
    }

};