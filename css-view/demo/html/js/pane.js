Vue.component('pane',{
    name: 'pane',
    template:`
        <div class="pane" v-show="show">
            <slot></slot>
        </div>
    `,
    props: {
        name: {
            type: String
        },
        // 标签页标题
        label: {
            type: String,
            default: ''
        }
    },
    // show 控制panel是否显示
    data: function () {
        return {
            show: true
        }
    },
    // props的label用户是可以动态调整的,所以在pane初始化以及label更新时，都要通知父组件更新
    methods: {
        updateNav() {
            this.$parent.updateNav();
        }
    },
    watch: {
        label() {
            this.updateNav();
        }
    },
    mounted: function () {
        this.updateNav();
    }
});