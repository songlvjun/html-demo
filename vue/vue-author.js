/**
 * 开发vue的插件
 * */
(function () {

    // 向外暴露插件对象
    const Author = {};

    Author.install = function (Vue, options) {
        // 1. 添加全局方法或属性
        Vue.getDesc = function () {
            return "Hello! this software is belong to song lv jun";
        };

        // 2. 添加全局资源
        Vue.directive('my-trim', function (el, binding) {
              el.textContent = binding.value.trim();
        });

        // 4. 添加实例方法
        Vue.prototype.$getName = function () {
            return "song lv jun";
        }
    };

    // 向外暴露
    window.Author = Author;
})();
