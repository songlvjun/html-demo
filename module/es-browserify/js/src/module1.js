// 分别暴露
export function foo() {
    let msg = "es6 export foo()";
    return msg;
}

export function foo2() {
    console.log("es6 export foo2()");
}

/**
 * 求指定两个元素的和
 * @param num1
 * @param num2
 * @returns {*} 和
 */
export function sum(num1, num2) {
    return num1 + num2;
}

export let arr = [1, 2, 3, 4];