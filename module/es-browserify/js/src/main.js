// 引入其他模块 import xx from '路径'

import {foo, foo2} from './module1.js';
import {fun1, fun2, foo2 as fo} from './module2.js';
import module3 from "./module3.js";

foo();
foo2();
fun1();
fun2();

// 模块起别名
fo();

console.log(module3.msg);
console.log(module3.foo());
