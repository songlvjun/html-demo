// 统一暴露
function fun1() {
    console.log("es6 fun1");
}

function fun2() {
    console.log("es6 fun2");
}

function foo2() {
    console.log("module2 foo2()...");
}

export {fun1, fun2, foo2};