// 默认暴露 可以暴露任意数据类型, 暴露和接收是一致的
/*
   模块的默认值是指通过default关键字是指的单个变量、函数、或者类、只能为每个模块设置一个默认的导出值。
   导出时多次使用default关键字是一个语法错误。
*/
/*
export default () => {
    console.log("默认暴露.....");
}
*/
export default {
    msg : "默认暴露",
    foo() {
        console.log(this.msg);
    }
}


