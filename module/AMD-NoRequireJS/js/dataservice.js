/* 定义一个没有依赖的模块 */
(function f(window) {
    let name = "dataservice.js";
    function getName() {
        return name;
    }
    window.dataservice = {getName};
})(window);
