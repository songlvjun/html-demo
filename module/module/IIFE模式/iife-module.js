/**
 * IIFE模式: 匿名函数自调用(闭包)
 */
(function () {
    let msg = "module";
    let foo = () => {
        console.log("foo() ", msg);
    };
    // 对外暴露接口
    window.module3 = {foo};
})();