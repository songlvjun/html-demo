1. 下载browserify
   * 全局: npm install browserify -g
   * 局部: npm install browserify --save-dev
   
   * 开发依赖: 开发时帮助我们编译打包, 正式生产时与browserify没有关联了
   * 运行依赖: 正式生产时

2. 打包处理js:
   * browserify js/src/app.js -o js/dist/bundle.js
   * 页面引入: 
        <script type="text/javascript" src="js/dist/bundle.js"></script>
     