/**
 * 暴露一个对象
 * @type {{msg: string, foo(): void}}
 */
module.exports = {
    msg: "Hello world Node.js",
    foo () {
        console.log(this.msg);
    }
};