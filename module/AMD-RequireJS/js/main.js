(function () {

    // require.js全局配置
    requirejs.config({
        baseUrl: 'js/',
        paths: {
            dataservice: './modules/dataservice',
            alertu: './modules/alertu',
            jquery: './libs/jquery.min',
        }
    });

    requirejs(['alertu'],function (alertu) {
        alertu.showMsg();
    });
})();