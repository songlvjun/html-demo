/*定义有依赖的模块 */
define(['dataservice', 'jquery'], function (dataservice, $) {
    let msg = "alteru.js is using dataservice.js、require.js";

    function showMsg() {
        console.log("song", msg, dataservice.getName());
    }

    $("body").css("background", "lemonchiffon");
    // 暴露模块
    return {showMsg};
});