1. 下载require.js 并引入
   * 官网: https://requirejs.org/
   * github: https://github.com/requirejs/requirejs
2. ### 创建项目结构:
   ``` 
    |- js
       |-- libs
           |--- require.js
       |-- modules
           |--- alter.js
           |--- dataservice.js 
       |-- index.js
    |- index.html
   ```
3. ### 规范:
   * 专门用于浏览器端, 模块的加载是异步的
4. ### 定义模块:
   定义没有依赖的模块
   * define(function(){
        return 模块
     });
   定义有依赖的模块
   * define(['mod1','mod2'], function(mod1, mod2) {
          // 使用mod1, mod2
          return 模块
     }); 
5. ### 使用模块
   requireJs(['mod1','mod2'], function(mod1, mod2) {
   });  