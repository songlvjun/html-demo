
// 引入第三方库
let uniq = require('uniq');

// 将其他的模块汇聚在主模块
let module1 = require('./modules/module1');
let module2 = require('./modules/module2');
let module3 = require('./modules/module3');


// 对象调用
module1.foo();
// 函数调用
module2();
module3.foo3();
module3.foo4();

let result = uniq(module3.arr);
console.log("uniq 第三方引入: " , result); // 12 2 4 5
